﻿#include "plochointeractorstyle.h"

void PlochoInteractorStyle::init(vtkSmartPointer<vtkRenderer> renderer, std::vector<int> *points)
{
	this->renderer = renderer;
	pointPicker = vtkSmartPointer<vtkPointPicker>::New();
	this->Interactor->SetPicker(pointPicker);
	this->Interactor->StartPickCallback();
	cubeSource = vtkSmartPointer<vtkCubeSource>::New();
	cubeSource->SetCenter(0, 0, 0);
	cubeSource->SetXLength(0.5);
	cubeSource->SetZLength(0.5);
	cubeSource->SetYLength(0.5);
	cubeMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	cubeMapper->SetInputConnection(cubeSource->GetOutputPort());
	cubeActor = vtkSmartPointer<vtkActor>::New();
	cubeActor->SetMapper(cubeMapper);
	cubeActor->PickableOff();
	cubeActor->GetProperty()->SetOpacity(0.6);
	cubeActor->GetProperty()->SetColor(1, 1, 1);
	selPoints = points;
}

void PlochoInteractorStyle::UpdatePointPositions(vtkSmartPointer<vtkPolyData> data)
{
	for (int i = 0; i < selPoints->size(); i++)
	{
		double point[3];
		renderer->RemoveActor(kocky[i]);
		data->GetPoint((*selPoints)[i], point);
		kocky[i]->SetPosition(point);
		renderer->AddActor(kocky[i]);
	}
}

void PlochoInteractorStyle::ClearPoints()
{
	std::cout << "vymazavam vybrane body" << std::endl;
	selPoints->clear();
	for (int i = 0; i < kocky.size(); i++)
	{
		renderer->RemoveActor(kocky[i]);
	}
	kocky.clear();
}

void PlochoInteractorStyle::OnLeftButtonDown()
{
	leftDown = true;
	vtkInteractorStyleRubberBandPick::OnLeftButtonDown();
}
void PlochoInteractorStyle::OnLeftButtonUp()
{
	leftDown = false;
	if (!dragging)
	{
		int pointID = SelectPoint();
		if(pointID>=0)std::cout << "vybral som bod: " << pointID << std::endl;
		
		selPoints->push_back(pointID);
	}
	vtkInteractorStyleRubberBandPick::OnLeftButtonUp();
}
int PlochoInteractorStyle::SelectPoint()
{
	double distance = renderer->GetActiveCamera()->GetDistance();
	double tol = 0.0015 / (distance / 1000);
	pointPicker->SetTolerance(tol);
	pointPicker->Pick(this->mouseX, this->mouseY, 0.0, renderer);
	int pointID = pointPicker->GetPointId();
	if (pointID >= 0) {
		selActor = vtkSmartPointer<vtkActor>::New();
		selActor->SetMapper(cubeMapper);
		selActor->PickableOff();
		selActor->GetProperty()->SetOpacity(0.6);
		selActor->GetProperty()->SetColor(1, 0, 0);
		double pointPos[3];
		pointPicker->GetPickPosition(pointPos);
		selActor->SetPosition(pointPos);
		renderer->AddActor(selActor);
		kocky.push_back(selActor);
		return pointID;
	}
	else
	{
		return -1;
	}
}

void PlochoInteractorStyle::OnRightButtonDown()
{
	rightDown = true;
	vtkInteractorStyleRubberBandPick::OnRightButtonDown();
}

void PlochoInteractorStyle::OnRightButtonUp()
{
	rightDown = false;
	if (!dragging)
	{
		ClearPoints();
	}
	vtkInteractorStyleRubberBandPick::OnRightButtonUp();
}

void PlochoInteractorStyle::OnMiddleButtonDown()
{
	midDown = true;
	vtkInteractorStyleRubberBandPick::OnMiddleButtonDown();
}

void PlochoInteractorStyle::OnMiddleButtonUp()
{
	midDown = false;
	vtkInteractorStyleRubberBandPick::OnMiddleButtonUp();
}

void PlochoInteractorStyle::OnKeyDown() {
	vtkInteractorStyleRubberBandPick::OnKeyDown();
}


void PlochoInteractorStyle::OnKeyUp() {
	vtkInteractorStyleRubberBandPick::OnKeyUp();
}

void PlochoInteractorStyle::OnKeyPress()
{
	vtkInteractorStyleRubberBandPick::OnKeyPress();
}

void PlochoInteractorStyle::OnMouseMove() {
	if (leftDown || rightDown || midDown)dragging = true;
	else dragging = false;
	vtkInteractorStyleRubberBandPick::OnMouseMove();
	this->setMouseCoordinates();
	if (!dragging)this->pickAPoint();
}


void PlochoInteractorStyle::OnMouseWheelForward()
{
	vtkInteractorStyleRubberBandPick::OnMouseWheelForward();
}


void PlochoInteractorStyle::OnMouseWheelBackward()
{
	vtkInteractorStyleRubberBandPick::OnMouseWheelBackward();
}

void PlochoInteractorStyle::OnChar()
{
	vtkInteractorStyleRubberBandPick::OnChar();
}

void PlochoInteractorStyle::setMouseCoordinates() {
	this->mouseX = this->Interactor->GetEventPosition()[0];
	this->mouseY = this->Interactor->GetEventPosition()[1];
}

void PlochoInteractorStyle::pickAPoint() {
	double distance = renderer->GetActiveCamera()->GetDistance();
	double tol = 0.0015 / (distance / 1000);
	pointPicker->SetTolerance(tol);
	pointPicker->Pick(this->mouseX, this->mouseY, 0.0, renderer);
	int pointID = pointPicker->GetPointId();
	if (pointID >= 0) {
		double pointPos[3];
		pointPicker->GetPickPosition(pointPos);
		cubeActor->SetPosition(pointPos);
		renderer->AddActor(cubeActor);
		//std::cout << "id vybraneho bodu: " << pointID << std::endl;
	}
	else
	{
		renderer->RemoveActor(cubeActor);
	}
	emit UpdateWidget();
}
