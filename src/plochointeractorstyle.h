﻿#pragma once
#ifndef PLOCHOINTERACTORSTYLE_HPP
#define PLOCHOINTERACTORSTYLE_HPP

#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkObjectFactory.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkPointPicker.h>
#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkCubeSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>

#include <QObject>

class PlochoInteractorStyle : public QObject, public vtkInteractorStyleRubberBandPick {
	Q_OBJECT

public:
	static PlochoInteractorStyle* New();
	vtkTypeMacro(PlochoInteractorStyle, vtkInteractorStyleRubberBandPick);
	void init(vtkSmartPointer<vtkRenderer> renderer, std::vector<int> *points);

public slots:
	void ClearPoints();
	void UpdatePointPositions(vtkSmartPointer<vtkPolyData>);

signals:
	void UpdateWidget();

private:
	vtkSmartPointer<vtkRenderer> renderer;
	vtkSmartPointer<vtkPointPicker> pointPicker;
	vtkSmartPointer<vtkActor> cubeActor;
	vtkSmartPointer<vtkActor> selActor;
	vtkSmartPointer<vtkCubeSource> cubeSource;
	vtkSmartPointer<vtkPolyDataMapper> cubeMapper;
	std::vector<int> *selPoints;
	std::vector<vtkSmartPointer<vtkActor>> kocky;
	void OnLeftButtonDown();
	void OnLeftButtonUp();
	void OnRightButtonDown();
	void OnRightButtonUp();
	void OnMiddleButtonDown();
	void OnMiddleButtonUp();
	void OnKeyDown();
	void OnKeyUp();
	void OnKeyPress();
	void OnMouseMove();
	void OnMouseWheelForward();
	void OnMouseWheelBackward();
	void OnChar();
	void setMouseCoordinates();
	void pickAPoint();
	int SelectPoint();
	bool leftDown = false, rightDown = false, midDown = false, dragging = false;
	int mouseX;
	int mouseY;
	
};

#endif // PLOCHOINTERACTORSTYLE_HPP